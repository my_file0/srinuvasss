import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router'
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  
  private toasterMsgSub=new Subject();
  constructor(private router:Router) { }
  
  fnSetToasterMsg(msg,clr){
       let dataObj={
         'msg':msg,
         'bg':clr
       }
      this.toasterMsgSub.next(dataObj); 
  }
  fnToasterObservable(){
      return this.toasterMsgSub.asObservable();
  }

  fnLogout(){
    sessionStorage.clear();
    this.router.navigateByUrl('login');
  }

  fnValidateFile(file){
      let extention=file.split('.').pop();
      if(extention == 'jpg'){
        return true;
      }
      return false;
  }
}
