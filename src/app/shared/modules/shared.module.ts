import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoaderComponent} from '../components/loader/loader.component'
import {ToasterComponent} from '../components/toaster/toaster.component'
import {RegisterComponent} from '../components/register/register.component'
import {FormsModule} from '@angular/forms';
import {ChangePwdComponent} from '../components/change-pwd/change-pwd.component'
import {TableComponent} from '../components/table/table.component';
import {PaginationComponent} from '../components/pagination/pagination.component'
import {MatMenuModule} from '@angular/material/menu';
@NgModule({
  declarations: [ChangePwdComponent,LoaderComponent,ToasterComponent,RegisterComponent,PaginationComponent,TableComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports:[
    LoaderComponent,
    ToasterComponent,
    RegisterComponent,
    FormsModule,
    PaginationComponent,
    TableComponent,
    ChangePwdComponent,
    MatMenuModule
  ]
})
export class SharedModule { }
