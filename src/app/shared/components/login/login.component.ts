import { Component, OnInit } from '@angular/core';
import {ServercallService} from '../../services/servercall.service'
import {Router} from '@angular/router';
import {SharedService} from '../../services/shared.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  dataObj={};
  isShowLoader=false;
  constructor(private shared: SharedService,private router:Router,private serverCall:ServercallService) { }

  ngOnInit() {
  }

  fnLogin(){
    console.log(this.dataObj);
    this.isShowLoader=true;
      this.serverCall.fnSendPostReq('users/login-check',{data:this.dataObj})
      .subscribe((res:any)=>{
           this.isShowLoader=false;
           if(res.length){
               let user=res[0];
               let role=user.role;
               sessionStorage.uid=user.uid;
               sessionStorage.pwd=btoa(user.pwd);
               sessionStorage.role=role;
               sessionStorage.isLoggedIn=true;
               sessionStorage.setItem('id',user._id);
              this.router.navigateByUrl(role);
           }else{
             this.shared.fnSetToasterMsg("Please check entered uid or pwd","red");
           }
      },()=>{
        this.isShowLoader=false;
        this.shared.fnSetToasterMsg("Something went wrong","red");

      })
  }

}
