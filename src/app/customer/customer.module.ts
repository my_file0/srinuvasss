import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { HomeComponent } from './home/home.component';
import { OrdersComponent } from './orders/orders.component';
import { CartComponent } from './cart/cart.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchComponent } from './search/search.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { SharedModule } from '../shared/modules/shared.module';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
@NgModule({
  declarations: [CustomerComponent, HomeComponent, OrdersComponent, CartComponent, ProfileComponent, SearchComponent, ProductlistComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    SharedModule,
    MatMenuModule,
    MatIconModule
    

  ]
})
export class CustomerModule { }
