import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CustomerComponent} from './customer.component'
import {ProfileComponent} from './profile/profile.component'
import {OrdersComponent} from './orders/orders.component'
import {CartComponent} from './cart/cart.component'
import { HomeComponent } from './home/home.component';



const routes: Routes = [{
  path:'',
  component:CustomerComponent,
  children:[
    {path:'',redirectTo:'home',pathMatch:'full'},
    {path:'home',component:HomeComponent},
    {path:'/cart',component:CartComponent}  ,
    {path:'/profile',component:ProfileComponent},
    {path:'orders',component:OrdersComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
